<!DOCTYPE html>
<head>
    <meta charset=utf-8" />
    <title>News</title>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    <link href="/template/css/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div id="wrapper">
	<div id="page">
            <div id="page-bgtop">
                <div id="page-bgbtm">
                    <div id="content">
                            <div class="post">
                                <h2 class="title"><a href='/news/<?php echo $newsItem['id'] ;?>'><?php echo $newsItem['title'].' # '.$newsItem['id'];?></a></h2>
                                    <p class="meta">Posted by <a href="#"><?php echo $newsItem['author_name'];?></a> on <?php echo $newsItem['date'];?>
							<a href='/news/' class="permalink"> Вернуться назад</a></p>
                                    <div class="entry">
							<p><img src="/template/images/pic01.jpg" width="550" height="300" alt="" /></p>
							<p><?php echo $newsItem['short_content'];?></p>
                                                        <p><?php echo $newsItem['content'];?></p>
                                    </div>
                            </div>
				<p><a href='/news/' class="permalink"> Вернуться к списку новостей</a></p>
                            <div style="clear: both;"></div>
                    </div>
                    <div style="clear: both;"></div>
		</div>
            </div>
	</div>
    </div>
</body>
</html>
