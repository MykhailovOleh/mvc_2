-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 21 2018 г., 03:56
-- Версия сервера: 5.7.21
-- Версия PHP: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `second_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `short_content` text NOT NULL,
  `content` text NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `date`, `short_content`, `content`, `author_name`, `preview`, `type`) VALUES
(52, 'Title', '2016-05-12 10:05:04', 'Short description', 'Full description', 'Oleh Mykhailov', 'images/2.png', 'NewsPublication'),
(53, 'Title', '2016-05-12 10:05:04', 'Short description', 'Full description', 'Oleh Mykhailov', 'images/2.png', 'NewsPublication'),
(54, 'Title', '2016-05-12 10:05:04', 'Short description', 'Full description', 'Oleh Mykhailov', 'images/2.png', 'NewsPublication'),
(55, 'Title', '2016-05-12 10:05:04', 'Short description', 'Full description', 'Oleh Mykhailov', 'images/2.png', 'NewsPublication'),
(56, 'Title', '2016-05-12 10:05:04', 'Short description', 'Full description', 'Oleh Mykhailov', 'images/2.png', 'NewsPublication');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
